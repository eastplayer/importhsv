const models = require("../database/models");
const standardApiReturn = require("../response/standard-api-return");
var path = require('path');
require('dotenv').config();
const Op = require('sequelize').Op;
const {import_struct} = require("../configs/import_struct");
const fs = require('fs-extra');
const uuid = require('uuid');
const formidable = require('formidable');
const Excel = require('exceljs');
const request = require('request-promise');

module.exports.importView = async (req, res) => {
    return res.render(path.join(__dirname,"../resources/import.html"),{
        APP_URL: process.env.APP_URL,
    });
}


module.exports.import = async (req, res) => {
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }
    let tables = Object.keys(import_struct);

    let uploadDir = __dirname + './../upload';

    let sampleFile = req.files.file;
    let filename = uuid.v1()+"."+ sampleFile.name.split(".")[1];
    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv(`${uploadDir}/${filename}`, function(err) {
        if (err)
          return res.status(500).send(err);
        tables.map(async (t)=>{
            await models.tracking_imports.create({
                file_name:filename,
                sheet_name:t,

            });
        })
        res.redirect('back');
    });
}

module.exports.process_uploaded_file = async () => {
    setInterval(async () => {
        let customer_sheet = await models.tracking_imports.findOne({where:{
            is_processing: false,
            sheet_name: 'member_accounts',
        }});

        let uploadDir = __dirname + '/../upload';

        if(customer_sheet){
            let file_path = `${uploadDir}/${customer_sheet.file_name}`;
            try{
                customer_sheet.is_processing = true;
                customer_sheet.save();
                var workbook = new Excel.Workbook(); 
                await workbook.xlsx.readFile(file_path);
                var worksheet = workbook.getWorksheet(customer_sheet.sheet_name);
                await worksheet.eachRow({ includeEmpty: false }, async function(row, rowNumber) {
                    try{
                        if(rowNumber==1 || rowNumber < customer_sheet.processing_line_index){return;}
                        let map_data = {};
                        import_struct.member_accounts.map((key,index)=>{
                            index = index + 1;
                            if(key && typeof row.values[index]!=="undefined"){
                                map_data[key] = row.values[index];
                            }
                        });
                        let customer = await models.customers.findOne({where:{
                            customer_id: map_data.ACCOUNT_NO
                        }});

                        if(!customer){
                            customer = await models.customers.create({
                                customer_id: map_data.ACCOUNT_NO,
                                data:map_data
                            });
                        }

                        map_data.STATUS = map_data.STATUS == 0 ? "ACTIVE" : "BLOCKED"; 
                        customer.data = map_data;
                        customer.save();
                        customer_sheet.processing_line_index = rowNumber;
                        customer_sheet.save();
                    }catch (e) {
                        console.log("catch error at saving customer",e);
                    }
                });
                //fs.unlink(file_path)
                customer_sheet.destroy();
            }catch (e) {
                customer_sheet.is_processing = false;
                customer_sheet.save();
                console.log("catch error at reading excel file",e);
            }
            return;
        }

        //==============================
        let contact_sheet = await models.tracking_imports.findOne({where:{
            is_processing: false,
            sheet_name: 'member_contacts',
        }});

        if(contact_sheet){
            let file_path = `${uploadDir}/${contact_sheet.file_name}`;
            try{
                contact_sheet.is_processing = true;
                contact_sheet.save();
                var workbook = new Excel.Workbook(); 
                await workbook.xlsx.readFile(file_path);
                var worksheet = workbook.getWorksheet(contact_sheet.sheet_name);
                await worksheet.eachRow({ includeEmpty: false }, async function(row, rowNumber) {
                    try{
                        if(rowNumber==1 || rowNumber < contact_sheet.processing_line_index){return;}
                        let map_data = {};
                        import_struct.member_contacts.map((key,index)=>{
                            index = index + 1;
                            if(key && typeof row.values[index]!=="undefined"){
                                map_data[key] = row.values[index];
                            }
                        });

                        let customer = await models.customers.findOne({where:{
                            customer_id: map_data.ACCOUNT_NO
                        }}); 

                        if(!customer){return;}

                        let contact = await models.contacts.findOne({where:{
                            contact_id: map_data.CONTACT_NO,
                            customer_id: customer.id
                        }});

                        if(!contact){
                            contact = await models.contacts.create({
                                contact_id: map_data.CONTACT_NO,
                                customer_id: customer.id,
                                data:map_data
                            });
                        }

                        map_data.STATUS = map_data.STATUS == 0 ? "ACTIVE" : "BLOCKED"; 
                        contact.data = map_data;
                        contact.save();
                        contact_sheet.processing_line_index = rowNumber;
                        contact_sheet.save();
                    }catch (e) {
                        console.log("catch error at saving contact",e);
                    }
                });
                fs.unlink(file_path)
                contact_sheet.destroy();
            }catch (e) {
                contact_sheet.is_processing = false;
                contact_sheet.save();
                console.log("catch error at reading excel file 2",e);
            }
            return;
        }
    },3000);
    

    
}



module.exports.pushToEventLogger = async () => {

    let offset = 0;
    const limit = 30;

    setInterval(async () => {
        console.log(`pushing to Event Logger, line: ${offset}`)
        let check = await models.tracking_imports.findOne({where:{
            is_processing:true,
        }});

        if(check){
            return;
        }

        let customers = await models.customers.findAll({
            where:{
                data: {
                    [Op.not]: null
                }
            }, 
            offset:offset,
            limit:limit
        });

        customers.map(async (customer)=>{
            let contacts = await models.contacts.findAll({where:{
                customer_id: customer.id
            }});

            if(contacts.length==0){return;}

            customer.dataValues.data.CONTACTS = contacts;

            request({
                method: 'POST',
                uri: process.env.EVENT_LOGGER_ENDPOINT,
                body: customer,
                json: true
            }).catch(function (err) {console.log(err)});
        });

        if(customers.length<offset){
            console.log("begin new push operation to Event Logger")
            offset=0;
        }else{
            offset+=limit;
        }
        console.log("End one operation to Event Logger");
    }, 3000);
    
}