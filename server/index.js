const express = require("express");
const path = require("path");
const routes = require("../routes");
const cors = require("cors");
const serverless = require("serverless-http");
const server = express();
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');

const {import_struct} = require("../configs/import_struct");
const uuid = require('uuid');



server.use(cors());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

server.engine('html', require('ejs').renderFile);
server.use(express.static(path.join(__dirname, "..", "..", "dist")));
server.use(routes);


module.exports = {
    serverless: serverless(server),
    default: server
};