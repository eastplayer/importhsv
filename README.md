### How to run
- run `cp .env.example .env` and fill out you .env
- run `yarn migrate` to sync data table and sequelize definition
- run `yarn start:dev`
- request /excel/import to start import operation, after uploading files to the server, there are jobs that automatically take care of the data.