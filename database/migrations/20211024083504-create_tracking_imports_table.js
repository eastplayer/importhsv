'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tracking_imports', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      file_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      sheet_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      processing_line_index: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      is_processing: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      }
    });

    await queryInterface.addConstraint('tracking_imports', ['file_name','sheet_name'], {
      type: 'unique',
      name: 'tracking_imports_unique_keys'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tracking_imports');
  }
};
