require('dotenv').config();

module.exports = {
    development: {
        url: process.env.DEV_DATABASE_URL,
        dialect: 'postgres',
        dialectOptions: {
            requestTimeout: 3000,
        },
    },
    test: {
        url: process.env.TEST_DATABASE_URL,
        dialect: 'postgres',
    },
    production: {
        url: process.env.DATABASE_URL,
        dialect: 'postgres',
        dialectOptions: {
            requestTimeout: 3000,
            ssl: {      /* <----- Add SSL option */
                require: true,
                rejectUnauthorized: false
              }
        },
    },
};
