'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TrackingImports extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  TrackingImports.init({
    file_name: DataTypes.STRING,
    sheet_name: DataTypes.STRING,
    processing_line_index: DataTypes.INTEGER,
    is_processing: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'tracking_imports',
  });
  return TrackingImports;
};