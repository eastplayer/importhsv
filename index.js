require('dotenv').config();
const ImportController = require("./controllers/ImportController");

const server = require('./server');

const PORT = process.env.PORT || 5006;

server.default.listen(PORT, () => console.log(`Server is live at localhost:${PORT}`));
ImportController.process_uploaded_file();
ImportController.pushToEventLogger();