const standardApiReturn = require("../response/standard-api-return");
const models = require("../database/models");
const Validator = require('validatorjs');

const validate_account = async (req, res, next) =>{
    let rules = {
        account_id: 'required|integer',
    };

    let check = new Validator(req.query, rules);
    if(check.passes()){
    	let transport = await models.transports.findOne({
	        where:{
	            id: req.query.account_id,
	            company_id:req.user.company_id,
	        }
	    });

	    if(!transport){
	        return res.status(412).json(standardApiReturn.apiReturnErrors("Account not found"));
	    }else{
	    	req.transport = transport;
	    	next(); 
	    }
    }else{
        return res.status(412).json(standardApiReturn.apiReturnErrors(check.errors));
    }
}

module.exports = {
	validate_account,
}