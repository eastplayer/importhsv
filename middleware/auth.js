const models = require("../database/models");
const jwt = require('jsonwebtoken');
const useragent = require('express-useragent');
const axios = require('axios');
const standardApiReturn = require("../response/standard-api-return");

require('dotenv').config();

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1].split(',')[0];
        jwt.verify(token, process.env.JWT_SECRET_KEY, async (err, payload) => {
            if (err) {
                return res.sendStatus(403).json(standardApiReturn.apiReturnErrors(err));
            }else{
                req.user = payload.user;
                next();
            }

            
        });
    } else {
        res.sendStatus(401);
    }
};



module.exports = {
    authenticateJWT,
}
