const {
    Router
} = require('express');
const importRoutes = require('./import-routes');
const router = Router();

router.use('/import', importRoutes);

module.exports = router;
