const {Router} = require('express');
const ImportController = require("../../controllers/ImportController");
const router = Router();
const Validator = require('validatorjs');
const standardApiReturn = require("../../response/standard-api-return");

//==================================================================


router.get('/', ImportController.importView);
router.post('/', ImportController.import);




module.exports = router;
