const {
    Router
} = require('express');
const excelRoutes = require('./excel');
const router = Router();
const fileUpload = require('express-fileupload');
router.use(fileUpload());

router.use('/excel', excelRoutes);

module.exports = router;
