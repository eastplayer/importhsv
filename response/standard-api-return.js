const apiReturnErrors = (errors) => {
	return {
		'error':1,
		'data':errors
	};
}

const apiReturnSuccess = (data) => {
	return {
		'error':0,
		'data':data
	};
}

module.exports = {
	apiReturnErrors,
	apiReturnSuccess
}