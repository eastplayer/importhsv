const import_struct = {
    member_accounts: ['ACCOUNT_NO','','ACCOUNT_TYPE','REF_TO','COMPANY','RANK','STATUS','BLOCKED_REASON','BLOCK_DATE','','CREATED_AT','','POINTS'],
    member_contacts: ['ACCOUNT_NO','CONTACT_NO','','','IS_MAIN','NAME','','','ADDRESS','','CITY','','EMAIL','','PHONE','COUNTRY','GENDER','DOB','','STATUS','BLOCKED_REASON','BLOCK_DATE','','CREATED_AT','']
} 

module.exports = {
    import_struct
}